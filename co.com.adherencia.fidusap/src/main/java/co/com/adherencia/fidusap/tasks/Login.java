package co.com.adherencia.fidusap.tasks;

import java.util.List;

import co.com.adherencia.fidusap.model.LoginForm;
import static co.com.adherencia.fidusap.userinterface.FidusapLoginPage.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;


public class Login implements Task {

     private List<LoginForm> loginForm;
	
	public Login(List<LoginForm> loginForm) {
		super();
		this.loginForm = loginForm;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(loginForm.get(0).getUser()).into(USER));
		actor.attemptsTo(Enter.theValue(loginForm.get(0).getPassword()).into(PASSWORD));
		actor.attemptsTo(Click.on(BUTTON_ENTER));
				
}

	
	public static Performable inTheApplication(List<LoginForm> loginForm) {
		return Tasks.instrumented(Login.class, loginForm );
	}

	
}
