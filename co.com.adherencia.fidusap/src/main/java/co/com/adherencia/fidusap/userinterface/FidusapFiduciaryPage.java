package co.com.adherencia.fidusap.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class FidusapFiduciaryPage {

	public static final Target DOCUMENTS_OPTION = Target.the("Documents option")
            .located(By.xpath("//li[@path='/Fidusap/modulodocumentos.psml']"));
	
	public static final Target TABLES_BASICS_OPTION = Target.the("Tables Basics option")
            .located(By.xpath("//li[@path='/Fidusap/modulotablasbasicas.psml']"));
	
}
