package co.com.adherencia.fidusap.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class FidusapHomePage {
	
	public static final Target BUTTON_FIDUCIARY = Target.the("Fiduciary button")
            .located(By.xpath("//a[@title='Fiduciaria']"));
	
	
}
