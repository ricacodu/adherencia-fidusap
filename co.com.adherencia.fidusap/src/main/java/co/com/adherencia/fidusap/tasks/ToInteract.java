package co.com.adherencia.fidusap.tasks;

import static co.com.adherencia.fidusap.userinterface.FidusapHomePage.*;
import static co.com.adherencia.fidusap.userinterface.FidusapFiduciaryPage.*;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class ToInteract implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(BUTTON_FIDUCIARY));
		actor.attemptsTo(Click.on(DOCUMENTS_OPTION));
		actor.attemptsTo(Click.on(TABLES_BASICS_OPTION));	
	}

	public static ToInteract withFidusap() {
		return Tasks.instrumented(ToInteract.class);
	}

}
