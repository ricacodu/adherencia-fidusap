package co.com.adherencia.fidusap.tasks;

import co.com.adherencia.fidusap.userinterface.FidusapLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class OpenTheSite implements Task {

	private FidusapLoginPage fidusapLoginPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(fidusapLoginPage));
		
		
	}
	
    public static OpenTheSite ofTheFidusap() {
		
		return Tasks.instrumented(OpenTheSite.class);
	}

}
