package co.com.adherencia.fidusap.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://192.227.109.189:12974/digitrust/ui/Fidusap")
public class FidusapLoginPage extends PageObject{

	public static final Target USER = Target.the("User")
            .located(By.name("org.apache.jetspeed.login.username"));
	
	public static final Target PASSWORD = Target.the("Password")
            .located(By.name("org.apache.jetspeed.login.password"));
	
	public static final Target BUTTON_ENTER = Target.the("Password")
            .located(By.xpath("//input[@value='Entrar']"));
	
		
}
