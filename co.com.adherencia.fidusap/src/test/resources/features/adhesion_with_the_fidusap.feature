#Author: rcorrale@bancolombia.com.co

@adhesion
Feature: Adhesion with fidusap
  I as a test analyst
  I need to test the adhesion with the fidusap application
  To identify if you can automate the tests in this application

  @HappyCase
  Scenario: Login to fidusap and interact with some iframe screens
    Given I logged in to the fidusap application
    |user     |password       |
    |rcorrale |Colombia2018++ |
    When Interact with buttons and iframe elements
    Then the application will have adhesion
 


