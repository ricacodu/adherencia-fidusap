package co.com.adherencia.fidusap.stepdefinitions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

import co.com.adherencia.fidusap.model.LoginForm;
import co.com.adherencia.fidusap.tasks.Login;
import co.com.adherencia.fidusap.tasks.OpenTheSite;
import co.com.adherencia.fidusap.tasks.ToInteract;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class ValidateAdherenceStepDefinitions {
	
	private static WiniumDriver driver = null;
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor ricardo = Actor.named("Ricardo");
	
	@Before
	public void configuracionInicial() throws MalformedURLException {
		ricardo.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^I logged in to the fidusap application$")
	public void iLoggedInToTheFidusapApplication(List<LoginForm> loginForm) throws Exception {
		ricardo.wasAbleTo(OpenTheSite.ofTheFidusap());
		ricardo.wasAbleTo(Login.inTheApplication(loginForm));
		
	}

	@When("^Interact with buttons and iframe elements$")
	public void interactWithButtonsAndIframeElements() throws Exception {
		ricardo.attemptsTo(ToInteract.withFidusap());
		DesktopOptions option = new DesktopOptions();
		option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
		driver = new WiniumDriver(new URL("http://localhost:9999"), option);
	}

	@Then("^the application will have adhesion$")
	public void theApplicationWillHaveAdhesion() throws Exception {
		
	}
	
	
}
