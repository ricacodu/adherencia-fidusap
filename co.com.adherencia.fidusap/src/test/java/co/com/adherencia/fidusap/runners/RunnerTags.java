package co.com.adherencia.fidusap.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/adhesion_with_the_fidusap.feature",
		tags= "@HappyCase",
		glue="co.com.adherencia.fidusap.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
